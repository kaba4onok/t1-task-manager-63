package ru.t1.rleonov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.rleonov.tm.api.service.dto.ITaskDTOService;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.enumerated.UserSort;
import ru.t1.rleonov.tm.exception.entity.EntityNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.repository.dto.TaskDTORepository;
import java.util.List;

@Service
@NoArgsConstructor
public class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO>
        implements ITaskDTOService {

    @NotNull
    @Autowired
    public TaskDTORepository repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable TaskDTO task = repository.findFirstByUserIdAndId(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public TaskDTO removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO task = repository.findFirstByUserIdAndId(userId, id);
        if (task == null) throw new EntityNotFoundException();
        repository.delete(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable TaskDTO task = repository.findFirstByUserIdAndId(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setStatus(status);
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId,
                           @Nullable final UserSort userSort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<TaskDTO> models;
        @Nullable Sort sort = Sort.by(Sort.Direction.ASC, "name");
        if (userSort == null) return repository.findAllByUserId(userId, sort);
        switch (userSort) {
            case BY_STATUS:
                sort = Sort.by(Sort.Direction.ASC, "status");
                break;
            case BY_CREATED:
                sort = Sort.by(Sort.Direction.ASC, "created");
                break;
            default:
                sort = Sort.by(Sort.Direction.ASC, "name");
        }
        return repository.findAllByUserId(userId, sort);
    }

}
