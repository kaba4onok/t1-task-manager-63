package ru.t1.rleonov.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.hibernate.cfg.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.rleonov.tm")
@EnableJpaRepositories("ru.t1.rleonov.tm.repository")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDbDriver());
        dataSource.setUrl(propertyService.getDbUrl());
        dataSource.setUsername(propertyService.getDbLogin());
        dataSource.setPassword(propertyService.getDbPassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.rleonov.tm.model", "ru.t1.rleonov.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getDbDialect());
        properties.put(Environment.DEFAULT_SCHEMA, propertyService.getDbSchema());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDbDdlAuto());
        properties.put(Environment.SHOW_SQL, propertyService.getDbShowSql());
        properties.put(Environment.FORMAT_SQL, propertyService.getDbFormatSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDbSecondLvlCash());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getDbFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getDbUseQueryCash());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getDbUseMinPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getDbRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDbHazelConfig());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
