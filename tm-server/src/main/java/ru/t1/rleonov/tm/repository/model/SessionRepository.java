package ru.t1.rleonov.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.model.Session;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractRepository<Session> {

}
