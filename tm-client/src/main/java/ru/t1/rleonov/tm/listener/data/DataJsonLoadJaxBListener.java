package ru.t1.rleonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.rleonov.tm.dto.request.ServerLoadDataJsonJaxBRequest;
import ru.t1.rleonov.tm.event.ConsoleEvent;

@Component
public final class DataJsonLoadJaxBListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-load-json-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Load data from json file using jaxb.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonLoadJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ServerLoadDataJsonJaxBRequest request = new ServerLoadDataJsonJaxBRequest(getToken());
        getDomainEndpoint().loadDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
