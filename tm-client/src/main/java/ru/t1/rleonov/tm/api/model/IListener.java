package ru.t1.rleonov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.event.ConsoleEvent;

public interface IListener {

    @Nullable
    String getArgument();

    String getDescription();

    String getName();

    void handler(@NotNull final ConsoleEvent consoleEvent);

}
