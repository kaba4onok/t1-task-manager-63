package ru.t1.rleonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.models.TaskWeb;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class TaskWebRepository {

    @Nullable
    private static final TaskWebRepository INSTANCE = new TaskWebRepository();

    public static TaskWebRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Map<String, TaskWeb> tasks = new LinkedHashMap<>();

    {
        add(new TaskWeb("FirstTask"));
        add(new TaskWeb("SecondTask"));
        add(new TaskWeb("ThirdTask"));
    }

    public void create() {
        add(new TaskWeb("New Task_" + System.currentTimeMillis()));
    }

    public void add(@NotNull final TaskWeb task) {
        tasks.put(task.getId(), task);
    }

    public void save(@NotNull final TaskWeb task) {
        tasks.put(task.getId(), task);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    @NotNull
    public Collection<TaskWeb> findAll() {
        return tasks.values();
    }

    @Nullable
    public TaskWeb findById(@NotNull final String id) {
        return tasks.get(id);
    }

}
