package ru.t1.rleonov.tm.servlet;

import ru.t1.rleonov.tm.repository.ProjectWebRepository;
import ru.t1.rleonov.tm.repository.TaskWebRepository;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/tasks/*")
public class TaskListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("tasks", TaskWebRepository.getInstance().findAll());
        req.setAttribute("projectRepository", ProjectWebRepository.getInstance());
        req.getRequestDispatcher("/WEB-INF/views/task-list.jsp").forward(req, resp);
    }

}
