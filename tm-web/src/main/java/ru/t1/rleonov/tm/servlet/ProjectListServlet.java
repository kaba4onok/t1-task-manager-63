package ru.t1.rleonov.tm.servlet;

import ru.t1.rleonov.tm.repository.ProjectWebRepository;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/projects/*")
public class ProjectListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("projects", ProjectWebRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/project-list.jsp").forward(req, resp);
    }

}
