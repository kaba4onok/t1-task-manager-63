package ru.t1.rleonov.tm.servlet;

import ru.t1.rleonov.tm.repository.TaskWebRepository;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/create/*")
public class TaskCreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TaskWebRepository.getInstance().create();
        resp.sendRedirect("/tasks");
    }

}
