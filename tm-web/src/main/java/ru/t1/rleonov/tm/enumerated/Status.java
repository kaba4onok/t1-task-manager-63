package ru.t1.rleonov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    @Getter
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
