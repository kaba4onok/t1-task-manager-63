package ru.t1.rleonov.tm.servlet;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.models.ProjectWeb;
import ru.t1.rleonov.tm.repository.ProjectWebRepository;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @Nullable final ProjectWeb project = ProjectWebRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @Nullable final String description = req.getParameter("description");
        @Nullable final String statusValue = req.getParameter("status");
        @Nullable final Status status = Status.valueOf(statusValue);
        @NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        @NotNull final String dateStartValue = req.getParameter("dateStart");
        @NotNull final String dateFinishValue = req.getParameter("dateFinish");

        @NotNull final ProjectWeb project = new ProjectWeb();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setStatus(status);

        if (!dateStartValue.isEmpty()) project.setDateStart(simpleDateFormat.parse(dateStartValue));
        else project.setDateStart(null);
        if (!dateFinishValue.isEmpty()) project.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else project.setDateFinish(null);

        ProjectWebRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}
