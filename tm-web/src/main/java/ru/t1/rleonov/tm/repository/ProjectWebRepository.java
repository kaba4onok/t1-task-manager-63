package ru.t1.rleonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.models.ProjectWeb;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ProjectWebRepository {

    @NotNull
    private static final ProjectWebRepository INSTANCE = new ProjectWebRepository();

    public static ProjectWebRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Map<String, ProjectWeb> projects = new LinkedHashMap<>();

    {
        add(new ProjectWeb("FirstProject"));
        add(new ProjectWeb("SecondProject"));
        add(new ProjectWeb("ThirdProject"));
    }

    public void create() {
        add(new ProjectWeb("New Project " + System.currentTimeMillis()));
    }

    public void add(@NotNull final ProjectWeb project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull final ProjectWeb project) {
        projects.put(project.getId(), project);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    @NotNull
    public Collection<ProjectWeb> findAll() {
        return projects.values();
    }

    @Nullable
    public ProjectWeb findById(@NotNull final String id) {
        return projects.get(id);
    }

}
